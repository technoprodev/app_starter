<?php
namespace app_starter\assets_manager;

use yii\web\AssetBundle;

class RequiredAsset extends AssetBundle
{
	public $sourcePath = '@app_starter/assets';
    
    public $css = [
    	'style.css',
    ];
    
    public $js = [];
    
    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}