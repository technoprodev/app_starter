<?php
$params['app.name'] = 'Clove Medical';
$params['app.description'] = 'We care your health.';
$params['app.owner'] = 'Technopro';
$params['app.author'] = 'Technopro';
$params['user.passwordResetTokenExpire'] = 3600;

$config = [
    'id' => 'app_starter',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app_starter\controllers',
    'bootstrap' => [
        'log',
        function(){
            Yii::setAlias('@download-dev-file', Yii::$app->getRequest()->getBaseUrl() . '/upload/dev-file');
            Yii::setAlias('@upload-dev-file', dirname(dirname(__DIR__)) . '/app_starter/web/upload/dev-file');
        },
    ],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-app_starter',
        ],
        'user' => [
            'identityClass' => 'app_starter\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-app_starter', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'session-app_starter',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];

return $config;