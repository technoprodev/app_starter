<?php
namespace app_starter\controllers;

use Yii;
use app_starter\models\Dev;
use app_starter\models\DevExtend;
use app_starter\models\DevChild;
use yii\widgets\ActiveForm;

/**
 * DevController implements an highly advanced CRUD actions for Dev model.
 */
class DevController extends \technosmart\yii\web\Controller
{
    protected function findModel($id)
    {
        if (($model = Dev::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    protected function findModelChild($id)
    {
        if (($model = DevChild::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }
    
    public function actionDatatables()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'd.id',
                'd.id_combination',
                'd.id_user_defined',
                'dco.name AS link',
                'de.extend AS extend',
            ])
            ->from('dev d')
            ->join('LEFT JOIN', 'dev_category_option dco', 'dco.id = d.link')
            ->join('LEFT JOIN', 'dev_extend de', 'de.id_dev = d.id')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Dev::getDb());
    }

    public function actionDatatable()
    {
        $db = Dev::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('dev d')
                ->join('LEFT JOIN', 'dev_category_option dco', 'dco.id = d.link')
                ->join('LEFT JOIN', 'dev_extend de', 'de.id_dev = d.id');
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'd.id',
                'd.id_combination',
                'd.id_user_defined',
                'dco.name AS link',
                'de.extend AS extend',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    public function actionIndex($id = null)
    {
        if (!$id) {
            return $this->render('list', [
                'title' => 'List of Datas',
            ]);
        }

        $model['dev'] = $this->findModel($id);
        return $this->render('detail', [
            'model' => $model,
            'title' => 'Detail of ' . $model['dev']->id_combination,
        ]);
    }

    public function actionCreate()
    {
        $error = true;

        $model['dev'] = isset($id) ? $this->findModel($id) : new Dev();
        $model['dev_extend'] = isset($model['dev']->devExtend) ? $model['dev']->devExtend : new DevExtend();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['dev']->load($post);
            $model['dev_extend']->load($post);
            if (isset($post['DevChild'])) {
                foreach ($post['DevChild'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $devChild = $this->findModelChild($value['id']);
                        $devChild->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $devChild = $this->findModelChild(($value['id']*-1));
                        $devChild->isDeleted = true;
                    } else {
                        $devChild = new DevChild();
                        $devChild->setAttributes($value);
                    }
                    $model['dev_child'][] = $devChild;
                }
            }

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['dev']),
                    ActiveForm::validate($model['dev_extend'])
                );
                return $this->json($result);
            }

            $transaction['dev'] = Dev::getDb()->beginTransaction();

            try {
                if (!$model['dev']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $model['dev_extend']->id_dev = $model['dev']->id;
                if (!$model['dev_extend']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $error = false;

                if (isset($model['dev_child']) and is_array($model['dev_child'])) {
                    foreach ($model['dev_child'] as $key => $devChild) {
                        $devChild->id_dev = $model['dev']->id;
                        if (!$devChild->isDeleted && !$devChild->validate()) $error = true;
                    }

                    if ($error) {
                        throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                    }
                
                    foreach ($model['dev_child'] as $key => $devChild) {
                        if ($devChild->isDeleted) {
                            if (!$devChild->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$devChild->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $transaction['dev']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['dev']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            foreach ($model['dev']->devChildren as $key => $devChild)
                $model['dev_child'][] = $devChild;

            if ($model['dev']->isNewRecord) {
                $model['dev']->id_combination = $this->sequence('dev-id_combination');
                $model['dev_child'][] = new DevChild();
            }
        }

        if ($error)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Create New Data',
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionUpdate($id)
    {
        $error = true;

        $model['dev'] = isset($id) ? $this->findModel($id) : new Dev();
        $model['dev_extend'] = isset($model['dev']->devExtend) ? $model['dev']->devExtend : new DevExtend();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['dev']->load($post);
            $model['dev_extend']->load($post);
            if (isset($post['DevChild'])) {
                foreach ($post['DevChild'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $devChild = $this->findModelChild($value['id']);
                        $devChild->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $devChild = $this->findModelChild(($value['id']*-1));
                        $devChild->isDeleted = true;
                    } else {
                        $devChild = new DevChild();
                        $devChild->setAttributes($value);
                    }
                    $model['dev_child'][] = $devChild;
                }
            }

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['dev']),
                    ActiveForm::validate($model['dev_extend'])
                );
                return $this->json($result);
            }

            $transaction['dev'] = Dev::getDb()->beginTransaction();

            try {
                if (!$model['dev']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $model['dev_extend']->id_dev = $model['dev']->id;
                if (!$model['dev_extend']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $error = false;

                if (isset($model['dev_child']) and is_array($model['dev_child'])) {
                    foreach ($model['dev_child'] as $key => $devChild) {
                        $devChild->id_dev = $model['dev']->id;
                        if (!$devChild->isDeleted && !$devChild->validate()) $error = true;
                    }

                    if ($error) {
                        throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                    }
                
                    foreach ($model['dev_child'] as $key => $devChild) {
                        if ($devChild->isDeleted) {
                            if (!$devChild->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$devChild->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $transaction['dev']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['dev']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            foreach ($model['dev']->devChildren as $key => $devChild)
                $model['dev_child'][] = $devChild;

            if ($model['dev']->isNewRecord) {
                $model['dev']->id_combination = $this->sequence('dev-id_combination');
                $model['dev_child'][] = new DevChild();
            }
        }

        if ($error)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update Data ' . $model['dev']->id_combination,
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    ////

    public function actionTest()
    {
        // $a = new \technoprodev\debug\Debug();
        // $a->ddx();
        return $this->render('test');
        return $this->json(true);
    }

    protected function arrayDiff($current = [], $new = [])
    {
        /*$update_diff = [];
        $insert_diff = array_diff_ukey($new, $current, function($x, $y) use ($current, $new, &$update_diff) {
            if ($x == $y) {
                if ($current[$x] != $new[$x]) {
                    $update_diff[] = $new[$x];
                }

                return 0;
            }
            else return $x > $y ? 1 : -1;
        });*/
        $update_diff = count($new) > 0 ? array_intersect_key($new, $current) : [];
        $insert_diff = count($new) > 0 ? array_diff_key($new, $current) : [];
        $delete_diff = count($current) > 0 ? array_diff_key($current, $new) : [];

        return [$insert_diff, $update_diff, $delete_diff];
    }

    public function actionArrayDiff()
    {
        $current = [
            '0a' => 'nol',
            '1a' => 'satu',
            '2a' => 'dua',
            '3a' => 'tiga',
        ];

        $new = [
            '1a' => 'satu',
            '2a' => 'two',
            '3a' => 'three',
            '4a' => 'four',
        ];
        
        // Yii::$app->debug->debugx($this->arrayDiff($current, $new));

        $query = new \yii\db\Query();
        $query
            ->select('*')
            ->from('dev');
        $query->orWhere(['id'=>'12', 'id_user_defined'=>'syiwa']);
        $query->andWhere('1=1');
        $a = '';
        $query->andFilterWhere(['or', ['like', 'id', 12], ['like', 'id', $a]]);

        $result = $query->createCommand()->sql;
        Yii::$app->debug->debugx($result);
        Yii::$app->debug->debugx($query->all());
    }
}