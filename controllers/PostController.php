<?php
namespace app_starter\controllers;

use Yii;
use technosmart\yii\web\Controller;
use app_starter\models\Post;

class PostController extends Controller
{
    /*public function actionCreatedAt()
    {
        $model['post'] = Post::find()->where(['category' => 'Health'])->andWhere(['>=', 'id', '2071'])->indexBy('id')->orderBy(['id' => SORT_ASC])->all();

        $begin = \DateTime::createFromFormat('d/m/Y', '22/11/2018');
        $end = \DateTime::createFromFormat('d/m/Y', '30/01/2020');
        $range = \DateInterval::createFromDateString('1 day');
        $allInterval = new \DatePeriod($begin, $range, $end->modify('+1 day'));

        if (ob_get_level() == 0) ob_start();
        $i = 2071;
        foreach ($allInterval as $date) {
            $nowDate = $date->format('Y-m-d');
            $nextDate = $date->modify('+1 day')->format('Y-m-d');

            $rand_epoch = rand(strtotime($nowDate), strtotime($nextDate));
            $model['post'][$i]->created_at = date('Y-m-d H:i:s', $rand_epoch);
            $model['post'][$i]->save();
            echo $i . '<br>';
            ob_flush(); flush();
            $i++;
            
            $rand_epoch = rand(strtotime($nowDate), strtotime($nextDate));
            $model['post'][$i]->created_at = date('Y-m-d H:i:s', $rand_epoch);
            $model['post'][$i]->save();
            echo $i . '<br>';
            ob_flush(); flush();
            $i++;

            $rand_epoch = rand(strtotime($nowDate), strtotime($nextDate));
            $model['post'][$i]->created_at = date('Y-m-d H:i:s', $rand_epoch);
            $model['post'][$i]->save();
            echo $i . '<br>';
            ob_flush(); flush();
            $i++;
        }
        echo 'selesai';
        ob_end_flush();
        return 0;
    }*/

    public function actionImage($t = 'brokoli.jpg')
    {
        $name = '../other/img/post/' . $t;
        try {
            $fp = fopen($name, 'rb');
            header("Content-Type: image/png");
            header("Content-Length: " . filesize($name));

            return fpassthru($fp);
        } catch (\Throwable $e) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionIndex($t)
    {
        if (($model['post'] = Post::find()->where(['t' => $t, 'status' => 'Sedang Aktif'])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
        $model['related_post'] = Post::find()
            ->select(['id', 't', 'title', 'featured_image', 'featured_image_relative', 'created_at', 'updated_at'])
            ->where(['id' => Post::getRelatedPosts($model['post']->id), 'status' => 'Sedang Aktif'])
            ->all();
        return $this->render('detail', [
            'title' => $model['post']->title,
            'description' => $model['post']->description,
            'model' => $model,
        ]);
    }
    
    public function actionHealth($page = 1)
    {
        $model['post'] = Post::find()
            ->select(['id', 't', 'title', 'featured_image', 'featured_image_relative', 'created_at', 'updated_at'])
            ->where(['category' => 'Health', 'status' => 'Sedang Aktif'])
            ->orderBy(['id' => SORT_DESC])
            ->limit(30)
            ->offset(($page - 1) * 30)
            ->all();

        $lastPage = (function(){
            $query = new \yii\db\Query();
            $query
                ->select([
                    'count(*)',
                ])
                ->from('post p')
                ->where(['category' => 'Health', 'status' => 'Sedang Aktif'])
            ;
            $countPost = $query->scalar();

            return intdiv($countPost, 30) + ($countPost % 30 ? 1 : 0);
        })();

        return $this->render('list-post', [
            'title' => 'Health',
            'description' => 'All health tips',
            'model' => $model,
            'page' => $page,
            'lastPage' => $lastPage,
        ]);
    }
    
    public function actionDrug($page = 1)
    {
        $model['post'] = Post::find()->where(['category' => 'Health'])->limit(100)->offset($page*100)->orderBy(['title' => SORT_ASC])->all();

        return $this->render('list-az', [
            'title' => 'Drug',
            'description' => 'All drugs',
            'model' => $model,
        ]);
    }
}
