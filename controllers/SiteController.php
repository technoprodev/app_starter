<?php
namespace app_starter\controllers;

use Yii;
use technosmart\yii\web\Controller;
use app_starter\models\User;
use app_starter\models\UserExtend;
use app_starter\models\Login;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['logout'], true, ['@'], ['POST']],
            ]),
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionRedirectSlash($url = '')
    {
        $newUrl = rtrim($url, '/');
        if ($newUrl) $newUrl = '/' . $newUrl;
        return $this->redirect(Yii::$app->getRequest()->getBaseUrl() . $newUrl, 301);
    }
    
    //

    public function ractionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        }
        return $this->render('index', [
            'title' => 'Homepage',
            'description' => 'Kickstart your project with this starter app',
        ]);
    }

    public function xactionSignup()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $error = true;

        $model['user'] = new User(['scenario' => 'password']);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['user']->load($post);

            $transaction['user'] = User::getDb()->beginTransaction();

            try {
                $model['user']->status = 1;
                $model['user']->setPassword($model['user']->password);
                $model['user']->generateAuthKey();
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }

                $model['user_extend'] = new UserExtend();
                $model['user_extend']->id = $model['user']->id;
                $model['user_extend']->sex = 'Male';
                if (!$model['user_extend']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
                
                $transaction['user']->commit();
                Yii::$app->session->setFlash('success', 'Your account has been created. Check your email to confirm your registration.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            $error = true;
        }

        if ($error) {
            $this->layout = 'empty';
            return $this->render('signup', [
                'model' => $model,
                'title' => 'Signup',
            ]);
        }
        else {
            return $this->goBack();
        }
    }

    public function xactionLogin()
    {
        if (!Yii::$app->user->isGuest)
            return $this->goHome();

        $model['login'] = new Login(['scenario' => 'using-login']);
        
        if ($model['login']->load(Yii::$app->request->post()) && $model['login']->login()) {
            return $this->goBack();
        } else {
            $this->layout = 'empty';
            return $this->render('login', [
                'model' => $model,
                'title' => 'Login',
            ]);
        }
    }

    public function xactionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionIndex()
    {
        $page = 1;
        $model['post'] = \app_starter\models\Post::find()
            ->select(['id', 't', 'title', 'featured_image', 'featured_image_relative', 'created_at', 'updated_at'])
            ->where(['category' => 'Health', 'status' => 'Sedang Aktif'])
            ->orderBy(['id' => SORT_DESC])
            ->limit(30)
            ->offset(($page-1)*30)
            ->all();

        $model['postPreview'] = \app_starter\models\Post::find()
            ->select(['id', 't', 'title', 'featured_image', 'featured_image_relative', 'created_at', 'updated_at'])
            ->where(['category' => 'Health', 'status' => 'Sedang Aktif'])
            ->orderBy(['id' => SORT_DESC])
            ->limit(9)
            ->offset(($page-1)*9)
            ->all();

        return $this->render('//post/homepage', [
            'title' => 'Homepage',
            'description' => 'We Care Your Health',
            'model' => $model,
        ]);
    }
}