<?php

use app_starter\assets_manager\RequiredAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use technosmart\yii\widgets\Menu as MenuWidget;
use yii\widgets\Breadcrumbs;
use technosmart\models\Menu;

RequiredAsset::register($this);
$this->beginPage();

if (Yii::$app->session->hasFlash('success'))
    $this->registerJs(
        'fn.alert("Success", "' . Yii::$app->session->getFlash('success') . '", "success");',
        3
    );
if (Yii::$app->session->hasFlash('info'))
    $this->registerJs(
        'fn.alert("Info", "' . Yii::$app->session->getFlash('info') . '", "info");',
        3
    );
if (Yii::$app->session->hasFlash('warning'))
    $this->registerJs(
        'fn.alert("Warning", "' . Yii::$app->session->getFlash('warning') . '", "warning");',
        3
    );
if (Yii::$app->session->hasFlash('error'))
    $this->registerJs(
        'fn.alert("Error", "' . Yii::$app->session->getFlash('error') . '", "danger");',
        3
    );
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <?php if (YII_ENV == 'prod') : ?><!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-156851132-2"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-156851132-2');
        </script>
        <?php endif; ?><title><?= $this->title ? strip_tags($this->title) . ' | ' : null ?><?= Yii::$app->params['app.name'] ?><?= Yii::$app->params['app.description'] ? ' - ' . Yii::$app->params['app.description'] : null ?></title>
        <meta name="description" content="<?= $this->description ? $this->description : Yii::$app->params['app.description'] ?>">
        <link rel="icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico">
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
        <meta name="viewport" content="device-width, height=device-height, initial-scale=1, minimum-scale=1">
        <meta name="base-url" content="<?= yii\helpers\Url::home(true) ?>">
        <?= Html::csrfMetaTags() ?>
        <?php $this->head() ?>
    </head>

    <body>
    <?php $this->beginBody() ?>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- START @ALERT & CONFIRM -->
        <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="title-alert">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-alert"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="title-confirm">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-confirm"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default modal-yes" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-default modal-no" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /END @ALERT & CONFIRM -->

        <?php $menu['allMenu'] = [
            [
                'label' => 'HEALTH',
                'url' => ['post/health'],
            ],
            /*[
                'label' => 'Drugs',
                'url' => ['post/drug'],
            ],
            [
                'label' => 'Symptoms',
                'url' => ['site/index'],
            ],*/
        ]; ?>

        <header technoart-fixedonscroll="bg-lightest">
            <div class="hidden-sm-less">
                <div class="container border-bottom border-lighter border-thin">
                    <div class="pull-left padding-y-15">
                        <a href="<?= Yii::$app->urlManager->createUrl('site/index') ?>" class="a-nocolor">
                            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo-full.png" height="25px;" class="">
                        </a>
                    </div>

                    <nav>
                        <?= MenuWidget::widget([
                            'items' => $menu['allMenu'],
                            'options' => [
                                'class' => 'pull-right menu-x menu-space-y-15 menu-space-x-10 fs-14 text-gray
                                    menu-active-text-azure padding-y-5',
                            ],
                            'activateItems' => true,
                            'openParents' => true,
                            'parentsCssClass' => 'has-submenu',
                            'encodeLabels' => false,
                            'labelTemplate' => '<a>{label}</a>',
                            'hideEmptyItems' => true,
                        ]); ?>
                    </nav>
                </div>
            </div>
            <div class="visible-sm-less">
                <div class="text-center padding-y-10 bg-azure darker-20">
                    <a href="<?= Yii::$app->urlManager->createUrl('site/index') ?>" class="a-nocolor">
                        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo-full.png" height="30px;" class="bg-lightest padding-x-15 padding-y-5 rounded-lg">
                    </a>
                </div>
                <div class="bg-azure text-center">
                    <nav>
                        <?= MenuWidget::widget([
                            'items' => $menu['allMenu'],
                            'options' => [
                                'class' => 'menu-x menu-space-y-5 menu-space-x-10 fs-13 inline-block text-light-azure
                                    menu-active-text-lightest',
                            ],
                            'activateItems' => true,
                            'openParents' => true,
                            'parentsCssClass' => 'has-submenu',
                            'encodeLabels' => false,
                            'labelTemplate' => '<a>{label}</a>',
                            'hideEmptyItems' => true,
                        ]); ?>
                    </nav>
                </div>
            </div>
        </header>

        <main class="has-sidebar-left m-left">
            <div class="overlay-left" technoart-toggle=".has-sidebar-left" technoart-toggleclasses="toggle-left"></div>

            <nav class="sidebar-left border-right padding-y-15">
                <?php $menuItem['main'] = [
                    [
                        'label' => 'Dev List',
                        'url' => ['dev/index'],
                        'visible' => Menu::menuVisible('dev', 'index'),
                    ],
                    [
                        'label' => 'Dev Create',
                        'url' => ['dev/create'],
                        'visible' => Menu::menuVisible('dev', 'create'),
                    ],
                ]; ?>
                <?php
                    $menu['main'] = MenuWidget::widget([
                        'items' => $menuItem['main'],
                        'options' => [
                            'class' => 'menu-y menu-xs menu-active-bold submenu-active-bold menu-active-bg-light-blue menu-hover-darker-10 submenu-active-bg-light-blue',
                        ],
                        'activateItems' => true,
                        'openParents' => true,
                        'parentsCssClass' => 'has-submenu',
                        'encodeLabels' => false,
                        'labelTemplate' => '<a>{label}</a>',
                        'hideEmptyItems' => true,
                    ]);
                ?>

                <?php if ($menu['main']): ?>
                    <?= $menu['main'] ?>
                    <hr class="margin-y-15">
                <?php endif; ?>
            </nav>

            <div class="page-wrapper padding-x-30 m-padding-x-10">
                <?= $content ?>
                <div class="margin-top-30"></div>
            </div>
        </main>

        <footer>
            <div class="bg-darkest">
                <div class="container padding-y-30 m-text-center">
                  2018 - <?= date('Y'); ?> ©
                  <a href="<?= Url::to(['post/index', 't' => 'some-rights-reserved']) ?>" class="a-nocolor">Some Rights Reserved</a> |
                  <a href="<?= Url::to(['post/index', 't' => 'privacy-policy']) ?>" class="a-nocolor">Privacy Policy</a> |
                  <a href="<?= Url::to(['post/index', 't' => 'terms-of-service']) ?>" class="a-nocolor">Terms of Service</a> |
                  <a href="<?= Url::to(['post/index', 't' => 'contact-us']) ?>" class="a-nocolor">Contact Us</a>
                  <div class="pull-right m-pull-none m-margin-top-20 fs-italic">Clove Medical - We Care Your Health</div>
                </div>
              </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>