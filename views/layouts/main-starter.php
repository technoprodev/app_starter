<?php

use app_starter\assets_manager\RequiredAsset;
use yii\helpers\Html;
use technosmart\yii\widgets\Menu as MenuWidget;
use yii\widgets\Breadcrumbs;
use technosmart\models\Menu;

RequiredAsset::register($this);
$this->beginPage();

if (Yii::$app->session->hasFlash('success'))
    $this->registerJs(
        'fn.alert("Success", "' . Yii::$app->session->getFlash('success') . '", "success");',
        3
    );
if (Yii::$app->session->hasFlash('info'))
    $this->registerJs(
        'fn.alert("Info", "' . Yii::$app->session->getFlash('info') . '", "info");',
        3
    );
if (Yii::$app->session->hasFlash('warning'))
    $this->registerJs(
        'fn.alert("Warning", "' . Yii::$app->session->getFlash('warning') . '", "warning");',
        3
    );
if (Yii::$app->session->hasFlash('error'))
    $this->registerJs(
        'fn.alert("Error", "' . Yii::$app->session->getFlash('error') . '", "danger");',
        3
    );
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <title><?= $this->title ? strip_tags($this->title) . ' | ' : null ?><?= Yii::$app->params['app.name'] ?><?= Yii::$app->params['app.description'] ? ' - ' . Yii::$app->params['app.description'] : null ?></title>
        <meta name="description" content="<?= $this->description ? $this->description : Yii::$app->params['app.description'] ?>">
        <link rel="icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico">
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
        <meta name="viewport" content="device-width, height=device-height, initial-scale=1, minimum-scale=1">
        <meta name="base-url" content="<?= yii\helpers\Url::home(true) ?>">
        <?= Html::csrfMetaTags() ?>
        <?php $this->head() ?>
    </head>

    <body>
    <?php $this->beginBody() ?>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- START @ALERT & CONFIRM -->
        <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="title-alert">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-alert"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="title-confirm">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-confirm"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default modal-yes" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-default modal-no" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /END @ALERT & CONFIRM -->

        <header class="header-wrapper border-bottom">
            <div class="header-left padding-y-10 padding-x-15">
                <span technoart-toggle=".has-sidebar-left" technoart-toggleclasses="toggle-left" class="margin-right-5 hidden-sm-greater">☰</span>
                <a href="<?= Yii::$app->urlManager->createUrl("site/index") ?>" class="underline-none">
                    <?= Yii::$app->params['app.name'] ?>
                </a>
            </div>
            <div class="header-right clearfix padding-y-10 padding-x-15">
                <?php if (!Yii::$app->user->isGuest) : ?>
                    <a class="pull-right m-pull-none" data-method="post" href="<?= Yii::$app->urlManager->createUrl("site/logout") ?>">Logout</a>
                <?php endif; ?>
            </div>
        </header>

        <main class="has-sidebar-left">
            <div class="overlay-left" technoart-toggle=".has-sidebar-left" technoart-toggleclasses="toggle-left"></div>

            <nav class="sidebar-left border-right padding-y-15">
                <?php $menuItem['main'] = [
                    [
                        'label' => 'Dev List',
                        'url' => ['dev/index'],
                        'visible' => Menu::menuVisible('dev', 'index'),
                    ],
                    [
                        'label' => 'Dev Create',
                        'url' => ['dev/create'],
                        'visible' => Menu::menuVisible('dev', 'create'),
                    ],
                ]; ?>
                <?php
                    $menu['main'] = MenuWidget::widget([
                        'items' => $menuItem['main'],
                        'options' => [
                            'class' => 'menu-y menu-xs menu-active-bold submenu-active-bold menu-active-bg-light-blue menu-hover-darker-10 submenu-active-bg-light-blue',
                        ],
                        'activateItems' => true,
                        'openParents' => true,
                        'parentsCssClass' => 'has-submenu',
                        'encodeLabels' => false,
                        'labelTemplate' => '<a>{label}</a>',
                        'hideEmptyItems' => true,
                    ]);
                ?>

                <?php if ($menu['main']): ?>
                    <?= $menu['main'] ?>
                    <hr class="margin-y-15">
                <?php endif; ?>
            </nav>

            <div class="page-wrapper padding-x-30 m-padding-x-10">
                <div class="border-bottom border-lighter padding-y-20 fw-light fs-16">
                    <?= Html::decode($this->title) ?>
                </div>

                <div class="padding-y-30">
                    <?= $content ?>
                </div>
            </div>
        </main>

        <footer class="border-top padding-y-15 text-center">
            <span class="fs-18 text-azure fs-italic"><?= Yii::$app->params['app.owner'] ?> © <?= date('Y') ?></span>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>