<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

if (isset($title)) $this->title = $title;
if (isset($description)) $this->description = $description;

$error = false;
$errorMessage = '';
if ($model['login']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['login'], ['class' => '']);
}
?>

<div class="padding-top-60 m-padding-top-30">
    <div class="text-center">
        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo-inverse.png" width="50px;" class="bg-dark-azure shadow circle padding-5">
    </div>
    <div class="margin-top-20"></div>
    <div class="border-lighter border-thick shadow rounded-xs" style="max-width: 350px; width: 100%; margin-left: auto; margin-right: auto;">
        <div class="clearfix padding-15 border-bottom bg-azure text-center fs-18">
            <span class="f-italic"><?= $this->title ?></span>
        </div>
        <div class="padding-20 bg-lightest">
            <?php $form = ActiveForm::begin(['id' => 'app']); ?>

            <?= $form->field($model['login'], 'login', ['options' => ['class' => 'form-wrapper']])->begin(); ?>
                <?= Html::activeLabel($model['login'], 'login', ['class' => 'form-label']); ?>
                <?= Html::activeTextInput($model['login'], 'login', ['class' => 'form-text', 'maxlength' => true]); ?>
                <?= Html::error($model['login'], 'login', ['class' => 'help-block help-block-error']); ?>
            <?= $form->field($model['login'], 'login')->end(); ?>

            <?= $form->field($model['login'], 'password', ['options' => ['class' => 'form-wrapper']])->begin(); ?>
                <?= Html::activeLabel($model['login'], 'password', ['class' => 'form-label']); ?>
                <?= Html::activePasswordInput($model['login'], 'password', ['class' => 'form-text', 'maxlength' => true]); ?>
                <?= Html::error($model['login'], 'password', ['class' => 'help-block help-block-error']); ?>
            <?= $form->field($model['login'], 'password')->end(); ?>

            <?= $form->field($model['login'], 'rememberMe', ['options' => ['class' => 'form-wrapper']])->begin(); ?>
                <div class="checkbox">
                    <?= Html::activeCheckbox($model['login'], 'rememberMe', ['uncheck' => 0]); ?>
                </div>
                <?= Html::error($model['login'], 'rememberMe', ['class' => 'help-block help-block-error']); ?>
            <?= $form->field($model['login'], 'rememberMe')->end(); ?>

            <?php if ($error) : ?>
                <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                    <?= $errorMessage ?>
                </div>
            <?php endif; ?>

            <div class="form-group">
                <?= Html::submitButton('Login', ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="bg-lighter padding-y-10">
            <div class="text-center text-azure fs-18"><?= Yii::$app->params['app.name'] ?></div>
            <div class="text-center fs-12 margin-top-2"><?= Yii::$app->params['app.author'] ?> © <?= date('Y') ?></div>
        </div>
    </div>
</div>