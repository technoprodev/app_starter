<?php

use yii\helpers\Html;
use yii\helpers\Url;

if (isset($title)) $this->title = $title;
if (isset($description)) $this->description = $description;
?>

<div class="margin-top-30"></div>

<div class="container" style="max-width: 1140px; width: 100%; margin-left: auto; margin-right: auto;">
    <div class="fs-16 text-azure fw-bold">MOST READ ARTICLES</div>

    <hr>

    <div class="box box-equal box-break-sm box-gutter">
        <?php foreach ($model['postPreview'] as $key => $post) : ?>
        <a class="box-4 margin-bottom-15" href="<?= Url::to(['post/index', 't' => $post->t]) ?>">
            <div class="" style="background: url('<?= $post->image ? $post->image : (Yii::$app->getRequest()->getBaseUrl() . '/img/logo.png') ?>'); height: 240px; background-size: cover; background-position: center;">
            </div>
            <div class="">
                <div class="fs-20 m-fs-14 fw-bold text-grayest hover-text-azure"><?= substr($post->title, 0, 69) ?></div>
                <div class="margin-top-5"></div>
                <div class="text-gray">Last updated: <?= $post->updated_at ? $post->updated_at : $post->created_at ?></div>
            </div>
        </a>
        <?php endforeach; ?>
    </div>
    
    <hr class="margin-y-30 border-top border-light-orange">
</div>

<div class="container" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">
    <div class="fs-16 text-azure fw-bold">RECENT ARTICLES</div>

    <hr>

    <?php foreach ($model['post'] as $key => $post) : ?>
        <a class="box box-equal" href="<?= Url::to(['post/index', 't' => $post->t]) ?>">
            <div class="box-4 padding-0" style="background: url('<?= $post->image ? $post->image : (Yii::$app->getRequest()->getBaseUrl() . '/img/logo.png') ?>'); height: 120px; background-size: cover; background-position: center;">
            </div>
            <div class="box-8">
                <div class="fs-20 m-fs-14 fw-bold text-grayest hover-text-azure"><?= $post->title ?></div>
                <div class="margin-top-5"></div>
                <div class="text-gray">Last updated: <?= $post->updated_at ? $post->updated_at : $post->created_at ?></div>
            </div>
        </a>
        <div class="margin-top-10"></div>
    <?php endforeach; ?>

    <hr>

    <div class="box box-gutter">
        <div class="box-12">
            <a
                href="<?= Url::to(['post/health']) ?>"
                class="button button-md button-block text-azure border-azure hover-bg-azure"
            >Other Articles</a>
        </div>
    </div>
</div>