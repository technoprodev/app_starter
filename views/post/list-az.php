<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($description)) $this->description = $description;
?>

<div class="container" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">
    <h1><?= $title ?></h1>

    <hr>

    <p class="fs-14"><?= $description ?></p>

    <hr>

    <?php foreach ($model['post'] as $key => $post) : ?>
    	<div>
	        <div class="fs-20 m-fs-14 fw-bold"><?= $post->title ?></div>
	        <div class="margin-top-5"></div>
	        <div class="text-gray">Last updated: <?= $post->updated_at ? $post->updated_at : $post->created_at ?></div>
	        <div class="margin-top-10"></div>
    	</div>
    <?php endforeach; ?>

</div>