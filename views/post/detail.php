<?php

use yii\helpers\Html;
use yii\helpers\Url;

if (isset($title)) $this->title = $title;
if (isset($description)) $this->description = $description;
?>

<div class="container" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">
    <div>
        <h1><?= $model['post']->title ?></h1>

        <hr>

        <div class="text-left">
            <a href="http://www.facebook.com/sharer.php?u=<?= Yii::$app->urlManager->createAbsoluteUrl(['post/index', 't' => $model['post']->t]) ?>" target="_blank">
                <img style="width:25px;" src="https://simplesharebuttons.com/images/somacro/facebook.png" alt="Facebook" />
            </a>
            <a href="https://twitter.com/share?url=<?= Yii::$app->urlManager->createAbsoluteUrl(['post/index', 't' => $model['post']->t]) ?>&amp;text=<?= $model['post']->title ?>&amp;hashtags=clovemedical" target="_blank">
                <img style="width:25px;" src="https://simplesharebuttons.com/images/somacro/twitter.png" alt="Twitter" />
            </a>
            <a href="mailto:?Subject=<?= $model['post']->title ?>&amp;Body=I%20saw%20this%20and%20thought%20of%20you!%20 <?= Yii::$app->urlManager->createAbsoluteUrl(['post/index', 't' => $model['post']->t]) ?>">
                <img style="width:25px;" src="https://simplesharebuttons.com/images/somacro/email.png" alt="Email" />
            </a>
        </div>

        <hr>

        <p class="fs-14"><?= $model['post']->description ?></p>

        <hr>

        <?= $model['post']->content ?>

        <hr>

        <?php if ($model['post']->reference) :  ?>
        <p><strong>Reference</strong></p>
        <p><?= $model['post']->reference ?></p>
        <?php endif;  ?>

        <?php if ($model['post']->source) :  ?>
        <p><strong>Source</strong></p>
        <p><?= $model['post']->source ?></p>
        <?php endif;  ?>

        <hr>

        <div>
            Last updated: <?= $model['post']->updated_at ? $model['post']->updated_at : $model['post']->created_at ?>
        </div>
    </div>

    <hr class="margin-y-30 border-top border-light-orange">

    <div>
        <div class="fs-16 text-azure fw-bold">RELATED ARTICLES</div>

        <hr>

        <?php foreach ($model['related_post'] as $key => $post) : ?>
            <a class="box box-equal" href="<?= Url::to(['post/index', 't' => $post->t]) ?>">
                <div class="box-4 padding-0" style="background: url('<?= $post->image ? $post->image : (Yii::$app->getRequest()->getBaseUrl() . '/img/logo.png') ?>'); height: 120px; background-size: cover; background-position: center;">
                </div>
                <div class="box-8">
                    <div class="fs-20 m-fs-14 fw-bold text-grayest hover-text-azure"><?= $post->title ?></div>
                    <div class="margin-top-5"></div>
                    <div class="text-gray">Last updated: <?= $post->updated_at ? $post->updated_at : $post->created_at ?></div>
                </div>
            </a>
            <div class="margin-top-10"></div>
        <?php endforeach; ?>
    </div>
</div>