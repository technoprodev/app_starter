<?php

use yii\helpers\Html;
use yii\helpers\Url;

if (isset($title)) $this->title = $title;
if (isset($description)) $this->description = $description;
?>

<div class="margin-top-30"></div>

<div class="container" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">
    <div class="fs-16 text-azure fw-bold">HEALTH ARTICLES</div>

    <hr>

    <?php foreach ($model['post'] as $key => $post) : ?>
        <a class="box box-equal" href="<?= Url::to(['post/index', 't' => $post->t]) ?>">
            <div class="box-4 padding-0" style="background: url('<?= $post->image ? $post->image : (Yii::$app->getRequest()->getBaseUrl() . '/img/logo.png') ?>'); height: 120px; background-size: cover; background-position: center;">
            </div>
            <div class="box-8">
                <div class="fs-20 m-fs-14 fw-bold text-grayest hover-text-azure"><?= $post->title ?></div>
                <div class="margin-top-5"></div>
                <div class="text-gray">Last updated: <?= $post->updated_at ? $post->updated_at : $post->created_at ?></div>
            </div>
        </a>
        <div class="margin-top-10"></div>
    <?php endforeach; ?>

    <hr>

    <div class="box box-gutter">
        <div class="box-6">
            <a
                <?= $page == 1 || $page - 1 > $lastPage ? 'disabled ' : '' ?>
                href="<?= $page - 1 == 1 ? Url::to(['post/health']) : Url::to(['post/health', 'page' => $page - 1]) ?>"
                class="button button-md button-block text-azure border-azure hover-bg-azure"
            >Prev</a>
        </div>
        <div class="box-6">
            <a
                <?= $page >= $lastPage ? 'disabled ' : '' ?>
                href="<?= Url::to(['post/health', 'page' => $page + 1]) ?>"
                class="button button-md button-block text-azure border-azure hover-bg-azure"
            >Next</a>
        </div>
    </div>
</div>