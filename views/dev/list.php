<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($description)) $this->description = $description;

$this->registerJsFile('@web/app/dev/list.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<table class="datatables table table-striped table-hover table-nowrap">
    <thead>
        <tr class="text-dark">
            <th>Action</th>
            <th>Id Combination</th>
            <th>Id User Defined</th>
            <th>Link</th>
            <th>Extend</th>
        </tr>
        <tr class="dt-search">
            <th></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search id combination..."/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search id user defined..."/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search link..."/></th>
            <th><input type="text" class="form-text border-none padding-0" placeholder="search extend..."/></th>
        </tr>
    </thead>
</table>

<?php if ($this->context->can('create')): ?>
    <hr class="margin-y-15">
    <div>
        <?= Html::a('Create Data', ['create'], ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
    </div>
<?php endif; ?>