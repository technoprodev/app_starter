<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($description)) $this->description = $description;

$this->registerJsFile('@web/app/dev/form.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);

technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
technosmart\assets_manager\AutosizeAsset::register($this);
technosmart\assets_manager\FileInputAsset::register($this);
technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
technosmart\assets_manager\JqueryMaskedInputAsset::register($this);

//
$devChildren = [];
if (isset($model['dev_child']))
    foreach ($model['dev_child'] as $key => $devChild)
        $devChildren[] = $devChild->attributes;

$this->registerJs(
    'vm.$data.dev.virtual_category = ' . json_encode($model['dev']->virtual_category) . ';' .
    'vm.$data.dev.devChildren = vm.$data.dev.devChildren.concat(' . json_encode($devChildren) . ');' .
    // 'vm.$data.dev.devChildren = Object.assign({}, vm.$data.dev.devChildren, ' . json_encode($devChildren) . ');' .
    '',
    3
);

//
$errorMessage = '';
$errorVue = false;
if ($model['dev']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['dev'], ['class' => '']);
}

if ($model['dev_extend']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['dev_extend'], ['class' => '']);
}

if (isset($model['dev_child'])) foreach ($model['dev_child'] as $key => $devChild) {
    if ($devChild->hasErrors()) {
        $errorMessage .= Html::errorSummary($devChild, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-gutter">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
  
    <?php if ($errorMessage) : ?>
        <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="form-wrapper">
        <label class="form-label"><?= $model['dev']->attributeLabels()['id_combination'] ?></label>
        <?= Html::activeHiddenInput($model['dev'], 'id_combination', ['v-model' => 'dev.id_combination']); ?>
        <div class="form-static border-bottom"><?= $model['dev']->id_combination ?></div>
    </div>

    <?= $form->field($model['dev'], 'id_combination', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'id_combination', ['class' => 'form-label']); ?>
        <?= Html::activeTextInput($model['dev'], 'id_combination', ['class' => 'form-text', 'maxlength' => true, 'disabled' => true]); ?>
        <?= Html::error($model['dev'], 'id_combination', ['class' => 'form-info']); ?>
    <?= $form->field($model['dev'], 'id_combination')->end(); ?>

    <?= $form->field($model['dev'], 'id_combination', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'id_combination', ['class' => 'form-label']); ?>
        <?= Html::activeTextInput($model['dev'], 'id_combination', ['class' => 'form-text', 'maxlength' => true, 'readonly' => true]); ?>
        <?= Html::error($model['dev'], 'id_combination', ['class' => 'form-info']); ?>
    <?= $form->field($model['dev'], 'id_combination')->end(); ?>

    <?= $form->field($model['dev'], 'id_user_defined', ['options' => ['class' => 'form-wrapper']/*, 'enableAjaxValidation' => true*/])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'id_user_defined', ['class' => 'form-label']); ?>
        <?= Html::activeTextInput($model['dev'], 'id_user_defined', ['class' => 'form-text', 'maxlength' => true, 'v-model' => 'dev.id_user_defined']); ?>
        <?= Html::error($model['dev'], 'id_user_defined', ['class' => 'form-info']); ?>
    <?= $form->field($model['dev'], 'id_user_defined')->end(); ?>

    <?= $form->field($model['dev'], 'text', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'text', ['class' => 'form-label']); ?>
        <?= Html::activeTextInput($model['dev'], 'text', ['class' => 'form-text', 'maxlength' => true, 'v-model' => 'dev.text']); ?>
        <?= Html::error($model['dev'], 'text', ['class' => 'form-info']); ?>
    <?= $form->field($model['dev'], 'text')->end(); ?>

    <?= $form->field($model['dev'], 'text', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'text', ['class' => 'form-label']); ?>
        <?= Html::activePasswordInput($model['dev'], 'text', ['class' => 'form-text', 'maxlength' => true, 'v-model' => 'dev.text']); ?>
        <?= Html::error($model['dev'], 'text', ['class' => 'form-info']); ?>
    <?= $form->field($model['dev'], 'text')->end(); ?>

    <?= $form->field($model['dev'], 'text', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'text', ['class' => 'form-label']); ?>
        <?= Html::activeTextInput($model['dev'], 'text', ['class' => 'form-text', 'maxlength' => true, 'v-model' => 'dev.text', 'list' => 'list-dev-autocomplete']); ?>
        <datalist id="list-dev-autocomplete">
            <?php
            $options = ArrayHelper::map(\app_starter\models\DevCategoryOption::find()->asArray()->all(), 'name', 'name');
            foreach ($options as $key => $value) { ?>
                <option value="<?= $key ?>"><?= $value ?></option>
            <?php } ?>
        </datalist>
        <?= Html::error($model['dev'], 'text', ['class' => 'form-info']); ?>
    <?= $form->field($model['dev'], 'text')->end(); ?>

    <?= $form->field($model['dev'], 'text', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'text', ['class' => 'form-label']); ?>
        <?= Html::activeTextArea($model['dev'], 'text', ['class' => 'form-textarea limited textarea-autosize', 'maxlength' => true, 'v-model' => 'dev.text']); ?>
        <?= Html::error($model['dev'], 'text', ['class' => 'form-info']); ?>
    <?= $form->field($model['dev'], 'text')->end(); ?>

    <hr>

    <?= $form->field($model['dev'], 'link', ['options' => ['class' => 'form-wrapper radio-elegant']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'link', ['class' => 'form-label']); ?>
        <?= Html::activeRadioList($model['dev'], 'link', ArrayHelper::map(\app_starter\models\DevCategoryOption::find()->indexBy('id')->asArray()->all(), 'id', 'name'), ['class' => 'form-radio box box-break-sm box-gutter', 'unselect' => null,
            'item' => function($index, $label, $name, $checked, $value){
                $checked = $checked ? 'checked' : '';
                $disabled = in_array($value, ['val1', 'val2']) ? 'disabled' : '';
                return "<label class='box-3'><input type='radio' name='$name' value='$value' $checked $disabled><i></i>$label</label>";

                // equal to this
                /*$disabled = in_array($value, ['val1', 'val2']) ? true : false;

                $radio = Html::radio($name, $checked, ['value' => $value, 'checked' => $checked, 'disabled' => $disabled]);
                return Html::tag('div', Html::label($radio . '<i></i>' . $label), ['class' => 'radio col-xs-3']);*/
            }]); ?>
        <?= Html::error($model['dev'], 'link', ['class' => 'form-info']); ?>
    <?= $form->field($model['dev'], 'link')->end(); ?>

    <?= $form->field($model['dev'], 'enum', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'enum', ['class' => 'form-label']); ?>
        <?= Html::activeRadioList($model['dev'], 'enum', $model['dev']->getEnum('enum'), ['class' => 'form-radio', 'unselect' => null,
            'item' => function($index, $label, $name, $checked, $value){
                $checked = $checked ? 'checked' : '';
                $disabled = in_array($value, ['val1', 'val2']) ? 'disabled' : '';
                return "<label><input type='radio' name='$name' value='$value' $checked $disabled><i></i>$label</label>";
            }]); ?>
        <?= Html::error($model['dev'], 'enum', ['class' => 'form-info']); ?>
    <?= $form->field($model['dev'], 'enum')->end(); ?>

    <?= $form->field($model['dev'], 'virtual_category', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'virtual_category', ['class' => 'form-label']); ?>
        <?= Html::activeCheckboxList($model['dev'], 'virtual_category', ArrayHelper::map(\app_starter\models\DevCategoryOption::find()->indexBy('id')->asArray()->all(), 'id', 'name'), ['class' => 'form-checkbox box box-break-sm box-gutter', 'unselect' => null,
            'item' => function($index, $label, $name, $checked, $value){
                $checked = $checked ? 'checked' : '';
                $disabled = in_array($value, ['val1', 'val2']) ? 'disabled' : '';
                return "<label class='col-xs-3'><input type='checkbox' name='$name' value='$value' $checked $disabled v-model='dev.virtual_category'><i></i>$label</label>";
            },
        ]); ?>
        <?= Html::error($model['dev'], 'virtual_category', ['class' => 'form-info']); ?>
    <?= $form->field($model['dev'], 'virtual_category')->end(); ?>

    <?= $form->field($model['dev'], 'set', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'set', ['class' => 'form-label']); ?>
        <?= Html::activeCheckboxList($model['dev'], 'set', $model['dev']->getSet('set'), ['class' => 'form-checkbox', 'unselect' => null,
            'item' => function($index, $label, $name, $checked, $value){
                $checked = $checked ? 'checked' : '';
                $disabled = in_array($value, ['val1', 'val2', 'technology']) ? 'disabled' : '';
                return "<label><input type='checkbox' name='$name' value='$value' $checked $disabled><i></i>$label</label>";
            }
        ]); ?>
        <?= Html::error($model['dev'], 'set', ['class' => 'form-info']); ?>
    <?= $form->field($model['dev'], 'set')->end(); ?>
    
    <?= $form->field($model['dev'], 'checklist', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'checklist', ['class' => 'form-label']); ?>
        <div class="form-checkbox">
            <?= Html::activeCheckbox($model['dev'], 'checklist', ['uncheck' => 0]); ?>
        </div>
        <?= Html::error($model['dev'], 'checklist', ['class' => 'form-info']); ?>
    <?= $form->field($model['dev'], 'checklist')->end(); ?>

    <?= $form->field($model['dev_extend'], 'extend', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['dev_extend'], 'extend', ['class' => 'form-label']); ?>
        <?= Html::activeTextInput($model['dev_extend'], 'extend', ['class' => 'form-text', 'maxlength' => true, 'v-model' => 'dev.textinput']); ?>
        <?= Html::error($model['dev_extend'], 'extend', ['class' => 'form-info']); ?>
    <?= $form->field($model['dev_extend'], 'extend')->end(); ?>

    <hr>
    
    <?= $form->field($model['dev'], 'link', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'link', ['class' => 'form-label']); ?>
        <?= Html::activeDropDownList($model['dev'], 'link', ArrayHelper::map(\app_starter\models\DevCategoryOption::find()->indexBy('id')->asArray()->all(), 'id', 'name', 'parent'), ['prompt' => 'Choose one please', 'class' => 'form-dropdown']); ?>
        <?= Html::error($model['dev'], 'link', ['class' => 'form-info']); ?>
    <?= $form->field($model['dev'], 'link')->end(); ?>

    <?= $form->field($model['dev'], 'enum', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'enum', ['class' => 'form-label']); ?>
        <?= Html::activeDropDownList($model['dev'], 'enum', $model['dev']->getEnum('enum'), ['prompt' => 'Choose one please', 'class' => 'form-dropdown']); ?>
        <?= Html::error($model['dev'], 'enum', ['class' => 'form-info']); ?>
    <?= $form->field($model['dev'], 'enum')->end(); ?>
    
    <?= $form->field($model['dev'], 'virtual_category', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'virtual_category', ['class' => 'form-label']); ?>
        <?= Html::activeListBox($model['dev'], 'virtual_category', ArrayHelper::map(\app_starter\models\DevCategoryOption::find()->indexBy('id')->asArray()->all(), 'id', 'name'), ['class' => 'form-dropdown', 'multiple' => true, 'unselect' => null, 'v-model' => 'dev.virtual_category']); ?>
        <?= Html::error($model['dev'], 'virtual_category', ['class' => 'form-info']); ?>
    <?= $form->field($model['dev'], 'virtual_category')->end(); ?>
    
    <?= $form->field($model['dev'], 'set', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'set', ['class' => 'form-label']); ?>
        <?= Html::activeListBox($model['dev'], 'set', $model['dev']->getSet('set'), ['class' => 'form-dropdown', 'multiple' => true, 'unselect' => null]); ?>
        <?= Html::error($model['dev'], 'set', ['class' => 'form-info']); ?>
    <?= $form->field($model['dev'], 'set')->end(); ?>

    <hr>

    <?= $form->field($model['dev'], 'virtual_file_upload', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'virtual_file_upload', ['class' => 'form-label']); ?>
        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
            <div class="form-text">
                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                <span class="fileinput-filename"><a href="<?= $model['dev']->virtual_file_download ?>"><?= $model['dev']->file ?></a></span>
            </div>
            <span class="input-group-addon btn btn-default btn-file">
                <span class="fileinput-new">Select file</span>
                <span class="fileinput-exists">Change</span>
                <?= Html::activeFileInput($model['dev'], 'virtual_file_upload'); ?>
            </span>
        </div>
        <?= Html::error($model['dev'], 'virtual_file_upload', ['class' => 'form-info']); ?>
    <?= $form->field($model['dev'], 'virtual_file_upload')->end(); ?>

    <hr>

    <?php if (isset($model['dev_child'])) foreach ($model['dev_child'] as $key => $value): ?>
        <?= $form->field($model['dev_child'][$key], "[$key]child")->begin(); ?>
            <?php /*Html::activeLabel($model['dev_child'][$key], "[$key]child", ['class' => 'form-label']);*/ ?>
            <?php /*Html::activeTextInput($model['dev_child'][$key], "[$key]child", ['class' => 'form-text', 'maxlength' => true]);*/ ?>
            <?php /*Html::error($model['dev_child'][$key], "[$key]child", ['class' => 'form-info']);*/ ?>
        <?= $form->field($model['dev_child'][$key], "[$key]child")->end(); ?>
    <?php endforeach; ?>

    <template v-if="typeof dev.devChildren == 'object'">
        <template v-for="(value, key, index) in dev.devChildren">
            <div v-show="!(value.id < 0)">
                <input type="hidden" v-bind:id="'devchild-' + key + '-id'" v-bind:name="'DevChild[' + key + '][id]'" type="text" v-model="dev.devChildren[key].id">
                <div v-bind:class="'form-wrapper field-devchild-' + key + '-child'">
                    <label v-bind:for="'devchild-' + key + '-child'" class="form-label">Child</label>
                    <div class="form-icon">
                        <i v-on:click="removeChild(key)" class="icon-append fa fa-close bg-light-red border-red margin-0 hover-pointer"></i>
                        <input v-bind:id="'devchild-' + key + '-child'" v-bind:name="'DevChild[' + key + '][child]'" class="form-text" type="text" v-model="dev.devChildren[key].child">
                    </div>
                    <div class="form-info"></div>
                </div>
            </div>
        </template>
    </template>

    <a v-on:click="addChild" class="button">Add Child</a>

    <hr class="margin-y-15">

    <?php if ($errorMessage) : ?>
        <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-wrapper clearfix">
        <?= Html::submitButton($model['dev']->isNewRecord ? 'Create' : 'Update', ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'button border-azure bg-lightest text-azure']); ?>
        <?= Html::a('Back to list', ['index'], ['class' => 'button border-azure bg-lightest text-azure hover-bg-azure pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>