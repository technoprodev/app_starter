<?php

echo(
    '<br>' .
    '@yii: ' . Yii::getAlias('@yii') . '<br>' .
    '@app: ' . Yii::getAlias('@app') . '<br>' .
    '@runtime: ' . Yii::getAlias('@runtime') . '<br>' .
    '@webroot: ' . Yii::getAlias('@webroot') . '<br>' .
    '@web: ' . Yii::getAlias('@web') . '<br>' .
    '@vendor: ' . Yii::getAlias('@vendor') . '<br>' .
    '@bower: ' . Yii::getAlias('@bower') . '<br>' .
    '@npm: ' . Yii::getAlias('@npm') . '<br>' .
    '@technosmart: ' . Yii::getAlias('@technosmart') . '<br>' .
    '@app_starter: ' . Yii::getAlias('@app_starter') . '<br>' .
    '@console: ' . Yii::getAlias('@console') . '<br>' .
    '__DIR__:' . __DIR__ . '<br>' .
    'dirname(__DIR__):' . dirname(__DIR__) . '<br>' .
    ''
);