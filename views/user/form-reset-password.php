<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($description)) $this->description = $description;

technosmart\assets_manager\Select2Asset::register($this);

$error = false;
$errorMessage = '';
if ($model['user']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['user'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>
    
<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['user'], 'password')->begin(); ?>
        <?= Html::activeLabel($model['user'], 'password', ['class' => 'control-label', 'label' => 'New Password']); ?>
        <?= Html::activePasswordInput($model['user'], 'password', ['class' => 'form-control', 'maxlength' => true]); ?>
        <?= Html::error($model['user'], 'password', ['class' => 'help-block']); ?>
    <?= $form->field($model['user'], 'password')->end(); ?>

    <?= $form->field($model['user'], 'password_repeat')->begin(); ?>
        <?= Html::activeLabel($model['user'], 'password_repeat', ['class' => 'control-label']); ?>
        <?= Html::activePasswordInput($model['user'], 'password_repeat', ['class' => 'form-control', 'maxlength' => true]); ?>
        <?= Html::error($model['user'], 'password_repeat', ['class' => 'help-block']); ?>
    <?= $form->field($model['user'], 'password_repeat')->end(); ?>

    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton('Update', ['class' => 'btn bg-azure border-azure hover-bg-light-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn text-azure border-azure hover-bg-light-azure']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn text-azure border-azure hover-bg-azure pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>