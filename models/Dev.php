<?php
namespace app_starter\models;

use Yii;

/**
 * This is the model class for table "dev".
 *
 * @property integer $id
 * @property string $id_combination
 * @property string $id_user_defined
 * @property string $created_at
 * @property string $updated_at
 * @property string $text
 * @property integer $link
 * @property string $enum
 * @property string $set
 * @property integer $checklist
 * @property string $file
 * @property string $time
 * @property string $date
 * @property string $datetime
 * @property string $year
 *
 * @property DevCategoryOption $link0
 * @property DevCategory[] $devCategories
 * @property DevChild[] $devChildren
 * @property DevExtend $devExtend
 */
class Dev extends \technosmart\yii\db\ActiveRecord
{
    public $virtual_category = [];
    public $virtual_file_upload;
    public $virtual_file_download;

    public static function tableName()
    {
        return 'dev';
    }

    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression("now()"),
            ],
        ];
    }

    public function rules()
    {
        return [
            //id

            //id_combination
            [['id_combination'], 'string', 'max' => 32],
            [['id_combination'], 'unique'],

            //id_user_defined
            [['id_user_defined'], 'string', 'max' => 32],
            [['id_user_defined'], 'unique'],

            //created_at
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],

            //text
            [['text'], 'string', 'max' => 32],

            //link
            [['link'], 'integer'],
            [['link'], 'exist', 'skipOnError' => true, 'targetClass' => DevCategoryOption::className(), 'targetAttribute' => ['link' => 'id']],

            //enum
            [['enum'], 'string'],

            //set
            [['set'], 'safe'],

            //checklist
            [['checklist'], 'integer'],

            //file
            [['file'], 'string', 'max' => 128],

            //time
            [['time'], 'safe'],

            //date
            [['date'], 'safe'],

            //datetime
            [['datetime'], 'safe'],

            //year
            [['year'], 'safe'],
            
            //virtual_category
            [['virtual_category'], 'safe'],
            
            //virtual_file_download
            [['virtual_file_download'], 'safe'],
            
            //virtual_file_upload
            [['virtual_file_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],

            /*[['text'], 'inlineValidator'],
            [['text'], function ($attribute, $params) {
                if ($this->$attribute == 'bandung') {
                    $this->addError($attribute, 'Bandung is not allowed here. Please give another value.');
                }
            }],
            [['text'], 'required',
                'when' => function ($model) { return $model->id_user_defined != 'jakarta'; },
                'whenClient' => "function (attribute, value) { return vm.$data.dev.id_user_defined != 'jakarta'; }",
                'message' => 'Dude, please.'
            ],*/
        ];
    }

    public function inlineValidator($attribute, $params) {
        if ($this->$attribute == 'bandung') {
            $this->addError($attribute, 'Bandung is not allowed here. Please give another value.');
        }
    }

    public function beforeValidate()
    {
        $this->virtual_poto_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_poto_upload');
        if ($this->virtual_poto_upload) {
            $this->poto = $this->virtual_poto_upload->name;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->virtual_file_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_file_upload');
        if ($this->virtual_file_upload) {
            if (!$insert) {
                $fileRoot = Yii::$app->params['configurations_file']['dev-file']['alias_upload_root'];
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->file;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->file = $this->virtual_file_upload->name;
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        \Yii::$app->db->createCommand()->delete('dev_category', 'id_dev = ' . (int) $this->id)->execute();
        if (is_array($this->virtual_category))
            foreach ($this->virtual_category as $key => $val) {
                $dc = new DevCategory();
                $dc->id_dev = $this->id;
                $dc->id_dev_category_option = $val;
                $dc->save();
            }

        if ($this->virtual_file_upload) {
            $uploadRoot = Yii::$app->params['configurations_file']['dev-file']['alias_upload_root'];
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_file_upload->saveAs($path . '/' . $this->file);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        \Yii::$app->db->createCommand()->delete('dev_category', 'id_dev = ' . (int) $this->id)->execute();
        \Yii::$app->db->createCommand()->delete('dev_extend', 'id_dev = ' . (int) $this->id)->execute();
        \Yii::$app->db->createCommand()->delete('dev_child', 'id_dev = ' . (int) $this->id)->execute();
        
        $fileRoot = Yii::$app->params['configurations_file']['dev-file']['alias_upload_root'];
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->file;
        if (is_file($filePath)) unlink($filePath);

        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        $this->virtual_category = \yii\helpers\ArrayHelper::getColumn(
            $this->getDevCategories()->asArray()->all(),
            'id_dev_category_option'
        );

        if($this->file) {
            $downloadBaseUrl = Yii::$app->params['configurations_file']['dev-file']['alias_download_base_url'];
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_file_download = $path . '/' . $this->file;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_combination' => 'Id Combination',
            'id_user_defined' => 'Id User Defined',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'text' => 'Text',
            'link' => 'Link',
            'enum' => 'Enum',
            'set' => 'Set',
            'checklist' => 'Checklist',
            'file' => 'File',
            'time' => 'Time',
            'date' => 'Date',
            'datetime' => 'Datetime',
            'year' => 'Year',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLink0()
    {
        return $this->hasOne(DevCategoryOption::className(), ['id' => 'link']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevCategories()
    {
        return $this->hasMany(DevCategory::className(), ['id_dev' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevChildren()
    {
        return $this->hasMany(DevChild::className(), ['id_dev' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevExtend()
    {
        return $this->hasOne(DevExtend::className(), ['id_dev' => 'id']);
    }
}
