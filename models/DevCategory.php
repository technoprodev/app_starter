<?php
namespace app_starter\models;

use Yii;

/**
 * This is the model class for table "dev_category".
 *
 * @property integer $id
 * @property integer $id_dev
 * @property integer $id_dev_category_option
 *
 * @property DevCategoryOption $devCategoryOption
 * @property Dev $dev
 */
class DevCategory extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'dev_category';
    }

    public function rules()
    {
        return [
            //id

            //id_dev
            [['id_dev'], 'required'],
            [['id_dev'], 'integer'],
            [['id_dev'], 'exist', 'skipOnError' => true, 'targetClass' => Dev::className(), 'targetAttribute' => ['id_dev' => 'id']],

            //id_dev_category_option
            [['id_dev_category_option'], 'required'],
            [['id_dev_category_option'], 'integer'],
            [['id_dev_category_option'], 'exist', 'skipOnError' => true, 'targetClass' => DevCategoryOption::className(), 'targetAttribute' => ['id_dev_category_option' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_dev' => 'Id Dev',
            'id_dev_category_option' => 'Id Dev Category Option',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevCategoryOption()
    {
        return $this->hasOne(DevCategoryOption::className(), ['id' => 'id_dev_category_option']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDev()
    {
        return $this->hasOne(Dev::className(), ['id' => 'id_dev']);
    }
}
