<?php
namespace app_starter\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $status
 * @property string $t
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $category
 * @property string $featured_image
 * @property string $featured_image_relative
 * @property string $author
 * @property string $reference
 * @property string $source
 * @property string $created_at
 * @property string $updated_at
 *
 * @property PostTag[] $postTags
 */
class Post extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'post';
    }

    public function rules()
    {
        return [
            //id

            //status
            [['status'], 'string'],

            //t
            [['t'], 'string', 'max' => 256],

            //title
            [['title'], 'string', 'max' => 256],

            //description
            [['description'], 'string'],

            //content
            [['content'], 'string'],

            //category
            [['category'], 'string', 'max' => 256],

            //featured_image
            [['featured_image'], 'string', 'max' => 256],

            //featured_image_relative
            [['featured_image_relative'], 'string', 'max' => 256],

            //author
            [['author'], 'string', 'max' => 256],

            //reference
            [['reference'], 'string'],

            //source
            [['source'], 'string'],

            //created_at
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            't' => 'T',
            'title' => 'Title',
            'description' => 'Description',
            'content' => 'Content',
            'category' => 'Category',
            'featured_image' => 'Featured Image',
            'featured_image_relative' => 'Featured Image Relative',
            'author' => 'Author',
            'reference' => 'Reference',
            'source' => 'Source',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostTags()
    {
        return $this->hasMany(PostTag::className(), ['post' => 'id']);
    }

    public function getImage()
    {
        if ($this->featured_image_relative) {
            return \yii\helpers\Url::to(['post/image', 't' => $this->featured_image_relative]);
        } else {
            return $this->featured_image;
        }
    }

    public static function getRelatedPosts($id)
    {
        $rawIds = Yii::$app->db->createCommand('
            SELECT pt.post, count(*) AS count
            FROM post_tag pt
            JOIN post p ON p.id = pt.post
            WHERE pt.tag IN (
                SELECT tag
                FROM post_tag
                WHERE post=:id
            )
            AND pt.post!=:id
            AND p.status = "Sedang Aktif"
            GROUP BY pt.post
            ORDER BY count DESC
            LIMIT 10
        ', ['id' => $id])->queryAll();

        $ids = [];
        foreach ($rawIds as $id) {
            $ids[] = $id['post'];
        }

        return $ids;
    }
}
