<?php
namespace app_starter\models;

use Yii;

/**
 * This is connector for Yii::$app->user with model User
 */
class User extends \technosmart\models\User
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            //username
            [['username'], 'required'],
            
            //email
            [['email'], 'required'],
        ]);
    }

    public static function findByLogin($login)
    {
        return static::find()
            ->join('INNER JOIN', 'user_extend ue', 'ue.id = user.id')
            ->where('username = :login or email = :login', [':login' => $login])
            ->andWhere(['status' => self::STATUS_ACTIVE])
            ->one();
    }

    public function getUserExtend()
    {
        return $this->hasOne(UserExtend::className(), ['id' => 'id']);
    }
}