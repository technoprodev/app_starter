<?php
namespace app_starter\models;

use Yii;

/**
 * This is the model class for table "dev_extend".
 *
 * @property integer $id_dev
 * @property string $extend
 *
 * @property Dev $dev
 */
class DevExtend extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'dev_extend';
    }

    public function rules()
    {
        return [
            //id_dev
            [['id_dev'], 'required'],
            [['id_dev'], 'integer'],
            [['id_dev'], 'exist', 'skipOnError' => true, 'targetClass' => Dev::className(), 'targetAttribute' => ['id_dev' => 'id']],

            //extend
            [['extend'], 'string', 'max' => 32],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_dev' => 'Id Dev',
            'extend' => 'Extend',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDev()
    {
        return $this->hasOne(Dev::className(), ['id' => 'id_dev']);
    }
}
