<?php
namespace app_starter\models;

use Yii;

/**
 * This is the model class for table "user_extend".
 *
 * @property integer $id
 * @property string $sex
 *
 * @property User $id0
 */
class UserExtend extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'user_extend';
    }

    public function rules()
    {
        return [
            //id
            [['id'], 'required'],
            [['id'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id' => 'id']],

            //sex
            [['sex'], 'required'],
            [['sex'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sex' => 'Sex',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }
}
