<?php
namespace app_starter\models;

use Yii;

/**
 * This is the model class for table "post_tag".
 *
 * @property integer $id
 * @property integer $post
 * @property string $tag
 *
 * @property Post $post0
 */
class PostTag extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'post_tag';
    }

    public function rules()
    {
        return [
            //id

            //post
            [['post'], 'required'],
            [['post'], 'integer'],
            [['post'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['post' => 'id']],

            //tag
            [['tag'], 'required'],
            [['tag'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post' => 'Post',
            'tag' => 'Tag',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost0()
    {
        return $this->hasOne(Post::className(), ['id' => 'post']);
    }
}
